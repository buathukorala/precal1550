## Welcome!

#### Here is the Course syllabus

- [MATH 1550-Sec 004](myfiles/MATH1550-sec004.pdf)
- [MATH 1550-Sec 006](myfiles/MATH1550-sec006.pdf)
- [MATH 1550-Sec 008](myfiles/MATH1550-sec008.pdf)


Head over to the [WebAssign](https://www.webassign.net/wa-auth/login).

Head over to the [Blackboard](https://ttu.blackboard.com/).

Here is the [Class Schedule and suggested practice problem numbers](myfiles/Math_1550_Course_Schedule_Fall_2019.pdf). 

Download the [Flash player](https://get.adobe.com/flashplayer/).

------

### Course Announcements
 
 - <span style="color:Gray;"> ALL HW sets are reopend until Dec 4th </span>
 - <span style="color:Red;"> HW 8.1, 10.2 - 10.4 are posted </span>
 - Here is the [solution key](myfiles/Review_4_solu.pdf) for Review 4.   
 - Here is the [review problem set](course_notes/Review_4.pdf) for Exam 4. 
 - <span style="color:Red;"> Exam 4: Wednesday 11/20/2019 </span>
 - <span style="color:Blue;"> Exam 4 Review session: Monday, Nov 18th Time: 6:15- 8:00PM  Classroom: TFPETR 110  </span>
 - <span style="color:Red;"> Trig Review session: Thursday, Nov 14th Time: 5:30- 7:00PM  Classroom: ELECE 00217  </span>
 - <span style="color:Red;"> HW 7.1, 7.2 and 7.3 are posted </span>
 - <span style="color:Gray;"> HW 6.5, and 6.6 are posted </span>
 - <span style="color:Gray;"> HW 5.3, 5.4,5.5, and 6.3 are posted </span>
 - <span style="color:Gray;"> ALL HW sets are reopend until Nov 4th </span>
 - <span style="color:Gray;"> Exam 3: Wednesday 10/30/2019 </span>
 - <span style="color:Gray;"> Review session: Monday, OCT 28th Time: 6:00-7:30PM  Classroom: TFPETR 110  </span>
 - Here are the [review problem set](myfiles/MATH_1550_Exam3_review.pdf) and the [solution key](myfiles/review3_solu.pdf). 
 - <span style="color:Gray;"> HW 5.1, and 5.2 are posted; due on 10/31/2019 </span>
 - <span style="color:Gray;"> HW 4.7, 6.1, and 6.2 are posted; due on 10/25/2019 </span>
 - <span style="color:Gray;"> HW 4.4, 4.5, and 4.6 are posted; due on 10/22/2019 </span>
 - <span style="color:Gray;"> HW 4.3 is posted; due on 10/18/2019 </span>
 - <span style="color:Gray;"> HW 4.2 is posted; due on 10/16/2019 </span>
 - <span style="color:Gray;"> HW 4.1 is posted; due on 10/16/2019 </span>
 - <span style="color:Gray;"> Here are the [review problem set](myfiles/Exam_2_review.pdf) and the [solution key](myfiles/Exam_2_review_solu.pdf)  </span>
 - <span style="color:Gray;"> Exam 2: Wednesday 10/09/2019: Study sections 1.8-3.1 </span>
 - <span style="color:Gray;"> HW 3.4 is posted; due on 10/11/2019 </span>
 - <span style="color:Gray;"> HW 3.3 is posted; due on 10/11/2019 </span>
 - <span style="color:Gray;"> HW 3.2 is posted; due on 10/09/2019 </span>
 - <span style="color:Gray;"> HW 3.1 is posted; due on 10/07/2019 </span>
 - <span style="color:Gray;"> HW 2.5 is posted; due on 10/03/2019 </span>
 - <span style="color:Gray;"> HW 2.4 is posted; due on 09/30/2019 </span>
 - <span style="color:Gray;"> Quiz 3: (Secs 004 and 008 - Friday, 09/27/2019; Sec 006 - Thursday, 09/26/2019) </span>. 
 - <span style="color:Gray;"> HW 2.1-2.3 are posted; due on 09/27/2019 </span>
 - <span style="color:Gray;">Exam 1: Wednesday 09/18/2019 </span>
 - Here are the [review problem set](myfiles/MATH_1550_Exam1_review.pdf) and the [solution key](myfiles/Review1_solu.pdf). 
 - <span style="color:Gray;"> HW 1.8 - 1.9 are posted; due on 09/20/2019 </span> 
 - <span style="color:Gray;"> HW 1.6 - 1.7 are posted; due on 09/17/2019 </span>
 - <span style="color:Gray;"> HW 1.4 - 1.5 are posted; due on 09/13/2019 </span>
 - <span style="color:Gray;"> HW 1.1 - 1.2 are posted; due on 09/10/2019 </span>
 - <span style="color:Gray;"> Quiz 2 - from Chapter 1.2 : (Sec 004, 006, and 008 - Thursday, 09/12/2019) </span> [solution key](myfiles/Quiz2_key.pdf). 
 - <span style="color:Gray;"> HW 1.3 is posted; due on 09/12/2019 </span>
 - <span style="color:Gray;"> Quiz 1: (Secs 004 and 008 - Friday, 09/06/2019; Sec 006 - Monday, 09/09/2019) </span>  [solution key](myfiles/Quiz1_key.pdf). 
 - <span style="color:Gray;"> HW A5: due on Wednesday, 09/04/2019 </span> 
 - <span style="color:Gray;"> HW A2: due on Tuesday, 09/03/2019 </span> 


------

### Class Notes

--------

Download the [Final Exam reviews](https://drive.google.com/file/d/1iPuIPjVXp5ZP7JlOuGGuTD5DoVUWQlUx/view?usp=sharing).


-------- 

 - [Chapter 10.4 (Nov 26)](course_notes/Ch_104.pdf)
 - [Chapter 10.3 (Nov 25)](course_notes/Ch_103.pdf)
 - [Chapter 10.2 (Nov 22)](course_notes/Ch_102.pdf)

-------- 

 - [Chapter 8.1 (Nov 19 and 21)](course_notes/Ch_81.pdf)
 

-------- 

Download the [Summary of Trigonometric Identities](https://jekyll.math.byuh.edu/courses/m111/handouts/summary_trigident.pdf).

 [Trig Review](course_notes/trig_review.pdf)
  

-------- 

 - [Chapter 7.4 (Nov 13-15)](course_notes/Ch_74.pdf)
 - [Chapter 7.3 (Nov 11 and 12)](course_notes/Ch_73.pdf)
 - [Chapter 7.1 and 7.2 (Nov 08)](course_notes/Ch_71.pdf)



-------- 
 
 - [Chapter 6.5 and 6.6 (Nov 06 and 07)](course_notes/Ch_65.pdf)
 - [Chapter 6.3 (Nov 01)](course_notes/Ch_63.pdf)
 
-------- 

 - [Chapter 5.5 (Oct 31 and Nov 1)](course_notes/Ch_55.pdf)
 - [Chapter 5.4 (Oct 29)](course_notes/Ch_54.pdf)
 - [Chapter 5.3 (Oct 28)](course_notes/Ch_53.pdf)
 - [Chapter 5.2 - Part 2 (Oct 25)](course_notes/Ch_52.pdf)
 - [Chapter 5.2 - Part 1 (Oct 25)](course_notes/Ch_52part1.pdf)
 - [Chapter 5.1 (Oct 24)](course_notes/Ch_51.pdf)


-------- 
 
 - [Chapter 6.2 (Oct 24)](course_notes/Ch_62.pdf)
 - [Chapter 6.1 (Oct 23 and 24)](course_notes/Ch_61.pdf)
 

-------- 
 - [Chapter 4.7 (Oct 22 and 23)](course_notes/Ch_47.pdf)
 - [Chapter 4.6 (Oct 21)](course_notes/Ch_46.pdf)
 - [Chapter 4.5 (Oct 18)](course_notes/Ch_45.pdf)
 - [Chapter 4.4 (Oct 16)](course_notes/Ch_44.pdf)
 - [Chapter 4.3 (Oct 15)](course_notes/Ch_43.pdf)
 - [Chapter 4.2 (Oct 14)](course_notes/Ch_42.pdf)
 - [Chapter 4.1 (Oct 11)](course_notes/Ch_41.pdf)

-------
 - [Chapter 3.5 (Oct 10)](course_notes/Ch_35.pdf)
 - [Chapter 3.4 (Oct 8)](course_notes/Ch_34.pdf)
 - [Chapter 3.3 (Oct 7)](course_notes/Ch_33.pdf)
 - [Chapter 3.2 (Oct 4)](course_notes/Ch_32.pdf)
 - [Chapter 3.1 (Oct 3)](course_notes/Ch_31.pdf)

------
 - [Chapter 2.6 (Oct 2 -3)](course_notes/Ch_26.pdf)
 - [Chapter 2.5 (Sep 30 & Oct 1)](course_notes/Ch_25.pdf)
 - [Chapter 2.4 (Sep 27)](course_notes/Ch_24.pdf)
 - [Chapter 2.3 (Sep 26)](course_notes/Ch_23.pdf)
 - [Chapter 2.2 (Sep 25)](course_notes/Ch_22.pdf)
 - [Chapter 2.1 (Sep 24)](course_notes/Ch_21.pdf)

------
 - [Chapter 1.9 (Sep 19 and 20)](course_notes/Ch_19.pdf)
 - [Chapter 1.8 (Sep 17 and 19)](course_notes/Ch_18.pdf)
 - [Exam 1: review (Sep 16)](myfiles/Review1_solu.pdf)
 - [Chapter 1.7 (Sep 13)](course_notes/Ch_17.pdf)
 - [Chapter 1.6 (Sep 12)](course_notes/Ch_16.pdf)
 - [Chapter 1.5 (Sep 11)](course_notes/Ch_15.pdf)
 - [Chapter 1.4 (Sep 10)](course_notes/Ch_14.pdf)
 - [Chapter 1.3 (Sep 9)](course_notes/Ch_13.pdf)
 - [Chapter 1.2 (Sep 05-06)](course_notes/Note_Sep_5__2019.pdf)
 - [Chapter 1.1 (Sep 04)](course_notes/Ch1p1.pdf)
 - [Appendix A2 and A5](course_notes/A2_and_A5.pdf)